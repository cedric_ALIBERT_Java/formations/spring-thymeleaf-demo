package fr.cedricalibert.springboot.thymeleafdemo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.cedricalibert.springboot.thymeleafdemo.model.Employee;

@Controller
@RequestMapping("/employees")
public class EmployeeController {
	
	private List<Employee> employees;
	
	@PostConstruct
	private void loadData() {
		Employee employee = new Employee(1, "test", "name", "email@email.com");
		Employee employee2 = new Employee(2, "test2", "name2", "email2@email.com");
		Employee employee3 = new Employee(3, "test3", "name3", "email3@email.com");
		
		employees = new ArrayList<>();
		
		employees.add(employee);
		employees.add(employee2);
		employees.add(employee3);
	}
	
	@GetMapping("/list")
	public String listEmployees(Model model) {
		model.addAttribute("employees", employees);
		
		return "list-employees";
	}
	
 	
}
